# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# mahmut özcan <faradundamarti@yandex.com>, 2020
# mahmut özcan <mahmutozcan@protonmail.com>, 2023
# Mehmet Akif 9oglu, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:47+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: Mehmet Akif 9oglu, 2023\n"
"Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: icewm-toolbar-icon-manager.sh:13
msgid "You are running an IceWM desktop"
msgstr "Bir IceWM masaüstü çalıştırıyorsunuz"

#: icewm-toolbar-icon-manager.sh:15 icewm-toolbar-icon-manager.sh:121
msgid "Warning"
msgstr "Uyarı"

#: icewm-toolbar-icon-manager.sh:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "Bu betiğin yalnız IceWM masaüstünde çalışması amaçlanmıştır"

#: icewm-toolbar-icon-manager.sh:77 icewm-toolbar-icon-manager.sh:86
#: icewm-toolbar-icon-manager.sh:102 icewm-toolbar-icon-manager.sh:135
#: icewm-toolbar-icon-manager.sh:155 icewm-toolbar-icon-manager.sh:335
msgid "Toolbar Icon Manager"
msgstr "Araç Çubuğu Simge Yöneticisi"

#: icewm-toolbar-icon-manager.sh:77
msgid "Help::TXT"
msgstr "Yardım::TXT"

#: icewm-toolbar-icon-manager.sh:77
msgid ""
"What is this?\\nThis utility adds and removes application icons to IceWm's toolbar.\\nThe toolbar application icons are created from an application's .desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\\nNote: some of antiX's applications are found in the sub-folder 'antiX'.\\n\n"
"TIM buttons:\\n 'ADD ICON' - select, from the list, the .desktop file of the application you want to add to your toolbar and it instantly shows up on the toolbar.\\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\\n'UNDO LAST STEP' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar\\n'MOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to select it and then move it to the left or to the right\\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearrange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\\n Warnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file!"
msgstr ""
"Bu nedir?\\nBu yardımcı program, uygulama simgelerini IceWm araç çubuğuna ekler ve kaldırır.\\nAraç çubuğu uygulama simgeleri, bir uygulamanın .desktop dosyasından oluşturulur.\\n.desktop dosyaları nedir?\\nGenellikle bir .desktop dosyası, uygulamanın tam adı, yürütülecek komutlar, kullanılacak simge, işletim sistemi menüsünde nereye yerleştirilmesi gerektiği gibi bilgilere sistemin kolay erişimini sağlamak için bir uygulamanın yükleme işlemi sırasında oluşturulur.\\nBir .desktop dosya adı genellikle uygulamanın adını gösterir ve bu da amaçlanan .desktop dosyasını bulmayı çok kolaylaştırır (örn: Firefox ESR’nin .desktop dosyası ‘firefox-esr.desktop’dur).\\nAraç çubuğuna yeni bir simge eklerken, kullanıcı ana pencerede sunulan alana tıklayabilir ayrıca yüklenen uygulamaların tüm .desktop dosyalarının bir listesi gösterilecektir.\\nAslında bu, araç çubuğuna eklenebilecek (neredeyse) tüm yüklü uygulamaların bir listesidir.\\nNot: Bazı antiX uygulamaları ‘antiX’ alt klasöründe bulunur.\\n\n"
"Bu nedir?\\nBu yardımcı program, uygulama simgelerini IceWm araç çubuğuna ekler ve kaldırır.\\nAraç çubuğu uygulama simgeleri, bir uygulamanın .desktop dosyasından oluşturulur.\\n.desktop dosyaları nedir?\\nGenellikle bir .desktop dosyası, uygulamanın tam adı, yürütülecek komutlar, kullanılacak simge, işletim sistemi menüsünde nereye yerleştirilmesi gerektiği gibi bilgilere sistemin kolay erişimini sağlamak için bir uygulamanın yükleme işlemi sırasında oluşturulur.\\nBir .desktop dosya adı genellikle uygulamanın adını gösterir ve bu da amaçlanan .desktop dosyasını bulmayı çok kolaylaştırır (örn: Firefox ESR’nin .desktop dosyası ‘firefox-esr.desktop’dur).\\nAraç çubuğuna yeni bir simge eklerken, kullanıcı ana pencerede sunulan alana tıklayabilir ayrıca yüklenen uygulamaların tüm .desktop dosyalarının bir listesi gösterilecektir.\\nAslında bu, araç çubuğuna eklenebilecek (neredeyse) tüm yüklü uygulamaların bir listesidir.\\nNot: Bazı antiX uygulamaları ‘antiX’ alt klasöründe bulunur.\\n\n"
"TIM düğmeleri:\\n ‘SİMGE EKLE’ - araç çubuğunuza eklemek istediğiniz uygulamanın .desktop dosyasını listeden seçin ve araç çubuğunda anında görünecektir.\\nHerhangi bir nedenle TIM, uygulamanız için doğru simgeyi bulamazsa, uygulamaya tıklayarak erişmeniz için öntanımlı ‘dişliler’ görüntüsünü kullanarak bir araç çubuğu simgesi oluşturmaya devam edecektir.\\nİlgili girişi elle düzenlemek ve uygulamanın simgesini değiştirmek için ‘Gelişmiş’ düğmesine tıklayabilirsiniz.\\n‘SON ADIMI GERİ AL’- araç çubuğuna her simge eklendiğinde veya kaldırıldığında, TIM bir yedekleme dosyası oluşturur. Bu düğmeye tıklarsanız, araç çubuğu herhangi bir onay olmadan yedekleme dosyasından anında geri yüklenir.\\n‘KALDIR SİMGESİ’ - bu, araç çubuğunda simgeleri olan tüm uygulamaların bir listesini gösterir. Araç çubuğundan simgesini kaldırmak için herhangi bir uygulamaya çift sol tıklayın\\n‘SİMGEYİ TAŞI’ - bu, araç çubuğunda simgeleri olan tüm uygulamaların bir listesini gösterir. Herhangi bir uygulamayı seçip çift sol tıklayın ve ardından sola veya sağa taşıyın\\n‘GELİŞMİŞ’ - tüm masaüstü araç çubuğu simge yapılandırmalarınızın bulunduğu metin yapılandırma dosyasını düzenlemenize izin verir. Bu dosyanın elle düzenlenmesi, kullanıcının simgelerin sırasını yeniden düzenlemesine ve herhangi bir simgeyi silmesine veya eklemesine olanak tanır. Dosya düzenleme için açılmadan önce metin yapılandırma dosyasının iç işleyişi hakkında kısa bir açıklama görüntülenir.\\n Uyarılar: Yapılandırma dosyasını yalnızca ne yaptığınızdan eminseniz elle düzenleyin! Bir yapılandırma dosyasını düzenlemeden önce daima bir yedek kopya oluşturun!"

#: icewm-toolbar-icon-manager.sh:86
msgid "Warning::TXT"
msgstr "Uyarı::TXT "

#: icewm-toolbar-icon-manager.sh:86
msgid ""
"If you click 'Yes', the toolbar configuration file will be opened for manual editing.\\n\n"
"How-to:\\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each toolbar icon entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nSave any changes and then restart IceWM.\\nYou can undo the last change from TIMs UNDO LAST STEP button."
msgstr ""
"Bu nedir?\\nBu yardımcı program, uygulama simgelerini IceWm araç çubuğuna ekler ve kaldırır.\\nAraç çubuğu uygulama simgeleri, bir uygulamanın .desktop dosyasından oluşturulur.\\n.desktop dosyaları nedir?\\nGenellikle bir .desktop dosyası, uygulamanın tam adı, yürütülecek komutlar, kullanılacak simge, işletim sistemi menüsünde nereye yerleştirilmesi gerektiği gibi bilgilere sistemin kolay erişimini sağlamak için bir uygulamanın yükleme işlemi sırasında oluşturulur.\\nBir .desktop dosya adı genellikle uygulamanın adını gösterir ve bu da amaçlanan .desktop dosyasını bulmayı çok kolaylaştırır (örn: Firefox ESR’nin .desktop dosyası ’firefox-esr.desktop’dur).\\nAraç çubuğuna yeni bir simge eklerken, kullanıcı ana pencerede sunulan alana tıklayabilir ayrıca yüklenen uygulamaların tüm .desktop dosyalarının bir listesi gösterilecektir.\\nAslında bu, araç çubuğuna eklenebilecek (neredeyse) tüm yüklü uygulamaların bir listesidir.\\nNot: Bazı antiX uygulamaları ’antiX’ alt klasöründe bulunur.\\n\n"
"TIM düğmeleri:\\n ’SİMGE EKLE’ - araç çubuğunuza eklemek istediğiniz uygulamanın .desktop dosyasını listeden seçin ve araç çubuğunda anında görünecektir.\\nHerhangi bir nedenle TIM, uygulamanız için doğru simgeyi bulamazsa, uygulamaya tıklayarak erişmeniz için öntanımlı ’dişliler’ görüntüsünü kullanarak bir araç çubuğu simgesi oluşturmaya devam edecektir.\\nİlgili girişi elle düzenlemek ve uygulamanın simgesini değiştirmek için ’Gelişmiş’ düğmesine tıklayabilirsiniz.\\n’SON ADIMI GERİ AL’- araç çubuğuna her simge eklendiğinde veya kaldırıldığında, TIM bir yedekleme dosyası oluşturur. Bu düğmeye tıklarsanız, araç çubuğu herhangi bir onay olmadan yedekleme dosyasından anında geri yüklenir.\\n’KALDIR SİMGESİ’ - bu, araç çubuğunda simgeleri olan tüm uygulamaların bir listesini gösterir. Araç çubuğundan simgesini kaldırmak için herhangi bir uygulamaya çift sol tıklayın\\n’SİMGEYİ TAŞI’ - bu, araç çubuğunda simgeleri olan tüm uygulamaların bir listesini gösterir. Herhangi bir uygulamayı seçip çift sol tıklayın ve ardından sola veya sağa taşıyın\\n’GELİŞMİŞ’ - tüm masaüstü araç çubuğu simge yapılandırmalarınızın bulunduğu metin yapılandırma dosyasını düzenlemenize izin verir. Bu dosyanın elle düzenlenmesi, kullanıcının simgelerin sırasını yeniden düzenlemesine ve herhangi bir simgeyi silmesine veya eklemesine olanak tanır. Dosya düzenleme için açılmadan önce metin yapılandırma dosyasının iç işleyişi hakkında kısa bir açıklama görüntülenir.\\n Uyarılar: Yapılandırma dosyasını yalnızca ne yaptığınızdan eminseniz elle düzenleyin! Bir yapılandırma dosyasını düzenlemeden önce daima bir yedek kopya oluşturun!"

#: icewm-toolbar-icon-manager.sh:87
msgid "Ok"
msgstr "Tamam"

#: icewm-toolbar-icon-manager.sh:102
msgid "Double click any Application to remove its icon:"
msgstr "Simgesini kaldırmak için herhangi bir uygulamayı çift tıklayın."

#: icewm-toolbar-icon-manager.sh:102
msgid "Remove"
msgstr "Kaldır"

#: icewm-toolbar-icon-manager.sh:113
msgid "file has something"
msgstr "dosyada birşeyler var"

#: icewm-toolbar-icon-manager.sh:119
msgid "file is empty"
msgstr "dosya boş"

#: icewm-toolbar-icon-manager.sh:121
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Hiç değişiklik yapılmadı!\\nİPUCU: Gelişmiş düğmesini her zaman "
"deneyebilirsiniz."

#: icewm-toolbar-icon-manager.sh:135
msgid "Double click any Application to move its icon:"
msgstr "Simgesini taşımak için herhangi bir uygulamayı çift tıklayın."

#: icewm-toolbar-icon-manager.sh:135
msgid "Move"
msgstr "Taşı"

#: icewm-toolbar-icon-manager.sh:145
msgid "nothing was selected"
msgstr "hiçbir şey seçilmedi"

#: icewm-toolbar-icon-manager.sh:155
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "$EXEC simgesiyle ne yapacağınızı seçin"

#: icewm-toolbar-icon-manager.sh:157
msgid "Move left"
msgstr "Sola taşı"

#: icewm-toolbar-icon-manager.sh:158
msgid "Move right"
msgstr "Sağa taşı"

#: icewm-toolbar-icon-manager.sh:223
msgid "Add selected app's icon"
msgstr "Seçilen uygulamanın simgesini ekle"

#: icewm-toolbar-icon-manager.sh:223
msgid "Choose application to add to the Toolbar"
msgstr "Araç Çubuğuna eklenecek uygulamayı seçin"

#: icewm-toolbar-icon-manager.sh:339
msgid "HELP!help:FBTN"
msgstr "YARDIM:help:FBTN"

#: icewm-toolbar-icon-manager.sh:340
msgid "ADVANCED!help-hint:FBTN"
msgstr "GELİŞMİŞ!help-hint:FBTN"

#: icewm-toolbar-icon-manager.sh:341
msgid "ADD ICON!add:FBTN"
msgstr "SİMGE EKLE!add:FBTN"

#: icewm-toolbar-icon-manager.sh:342
msgid "REMOVE ICON!remove:FBTN"
msgstr "SİMGEYİ KALDIR!remove:FBTN"

#: icewm-toolbar-icon-manager.sh:343
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "SİMGEYİ TAŞI!gtk-go-back-rtl:FBTN"

#: icewm-toolbar-icon-manager.sh:344
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "SON ADIMI GERİ AL!undo:FBTN"

#: icewm-toolbar-icon-manager.sh:345
msgid ""
"Please select any option from the buttons below to manage Toolbar icons"
msgstr ""
"Araç çubuğu simgelerini yönetmek için lütfen aşağıdaki düğmelerden herhangi "
"bir seçeneği seçin."
