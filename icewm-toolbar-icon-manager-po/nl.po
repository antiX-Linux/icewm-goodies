# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# anticapitalista <anticapitalista@riseup.net>, 2020
# James Bowlin <BitJam@gmail.com>, 2020
# Eadwine Rose, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:47+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: Eadwine Rose, 2023\n"
"Language-Team: Dutch (https://www.transifex.com/anticapitalista/teams/10162/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: icewm-toolbar-icon-manager.sh:13
msgid "You are running an IceWM desktop"
msgstr "U gebruikt een IceWM desktop"

#: icewm-toolbar-icon-manager.sh:15 icewm-toolbar-icon-manager.sh:121
msgid "Warning"
msgstr "Waarschuwing"

#: icewm-toolbar-icon-manager.sh:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr "Dit script kan alleen worden uitgevoerd in een IceWM desktop"

#: icewm-toolbar-icon-manager.sh:77 icewm-toolbar-icon-manager.sh:86
#: icewm-toolbar-icon-manager.sh:102 icewm-toolbar-icon-manager.sh:135
#: icewm-toolbar-icon-manager.sh:155 icewm-toolbar-icon-manager.sh:335
msgid "Toolbar Icon Manager"
msgstr "Werkbalk Icoon Beheer"

#: icewm-toolbar-icon-manager.sh:77
msgid "Help::TXT"
msgstr "Help::TXT"

#: icewm-toolbar-icon-manager.sh:77
msgid ""
"What is this?\\nThis utility adds and removes application icons to IceWm's toolbar.\\nThe toolbar application icons are created from an application's .desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is created during an application's installation process to allow the system easy access to relevant information, such as the app's full name, commands to be executed, icon to be used, where it should be placed in the OS menu, etc.\\nA .desktop file name usually refers to the app's name, which makes it very easy to find the intended .desktop file (ex: Firefox ESR's .desktop file is 'firefox-esr.desktop').\\nWhen adding a new icon to the toolbar, the user can click the field presented in the main window and a list of all the .desktop files of the installed applications will be shown.\\nThat, in fact, is a list of (almost) all installed applications that can be added to the toolbar.\\nNote: some of antiX's applications are found in the sub-folder 'antiX'.\\n\n"
"TIM buttons:\\n 'ADD ICON' - select, from the list, the .desktop file of the application you want to add to your toolbar and it instantly shows up on the toolbar.\\nIf, for some reason, TIM fails to find the correct icon for your application, it will still create a toolbar icon using the default 'gears' image so that you can still click to access the application.\\nYou can click the 'Advanced' button to manually edit the relevant entry and change the application's icon.\\n'UNDO LAST STEP' - every time an icon is added or removed from the toolbar, TIM creates a backup file. If you click this button, the toolbar is instantly restored from that backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to remove its icon from the toolbar\\n'MOVE ICON' - this shows a list of all applications that have icons on the toolbar. Double left click any application to select it and then move it to the left or to the right\\n'ADVANCED' - allows for editing the text configuration file that has all of your desktop's toolbar icon's configurations. Manually editing this file allows the user to rearrange the order of the icons and delete or add any icon. A brief explanation about the inner workings of the text configuration file is displayed before the file is opened for editing.\\n Warnings: only manually edit a configuration file if you are sure of what you are doing! Always make a back up copy before editing a configuration file!"
msgstr ""
"Wat is dit?\\nDit hulpprogramma voegt applicatie-iconen toe aan de werkbalk van IceWm en verwijdert ze.\\nDe applicatieiconen op de werkbalk worden gemaakt van het .desktop bestand van een applicatie.\\nWat zijn .desktop bestanden?\\nMeestal wordt een .desktop bestand gemaakt tijdens het installatieproces van een applicatie om het systeem gemakkelijk toegang te geven tot relevante informatie, zoals de volledige naam van de applicatie, de uit te voeren opdrachten, het te gebruiken icoon, de plaats waar het in het OS-menu moet worden geplaatst, etc.\\nEen .desktop bestandnaam verwijst meestal naar de naam van de app, waardoor het zeer eenvoudig is om het beoogde .desktop bestand te vinden (bv: Firefox ESR's .desktop-bestand is 'firefox-esr.desktop').\\nAls de gebruiker een nieuw icoon aan de werkbalk toevoegt, kan hij op het veld in het hoofdvenster klikken en een lijst van alle .desktop bestanden van de geïnstalleerde applicaties worden getoond.\\nDat is in feite een lijst van (bijna) alle geïnstalleerde applicaties die aan de werkbalk kunnen worden toegevoegd.\\n Opmerking: sommige van de antiX applicaties zijn te vinden in de submap 'antiX'.\\n\n"
"TIM-knoppen:\\n'ICOON TOEVOEGEN' - selecteer uit de lijst het .desktop-bestand van de applicatie die u aan de werkbalk wilt toevoegen en het verschijnt gelijk op de werkbalk.\\nAls TIM om een of andere reden niet het juiste icoon voor uw applicatie vindt, maakt het nog steeds een werkbalkicoon aan de hand van de standaard 'tandwielen'-afbeelding, zodat u nog steeds kunt klikken om toegang te krijgen tot de applicatie.\\nU kunt op de knop 'Geavanceerd' klikken om de relevante invoer handmatig te bewerken en het icoon van de applicatie te wijzigen.\\n'LAATSTE STAP ONGEDAAN MAKEN' - elke keer als een icoon wordt toegevoegd of verwijderd uit de werkbalk, maakt TIM een back-upbestand. Als u op deze knop klikt, wordt de werkbalk onmiddellijk hersteld van dat back-upbestand zonder enige bevestiging.\\n'VERWIJDER ICOON' - dit toont een lijst van alle toepassingen met iconen op de werkbalk. Dubbel linksklik op een toepassing om het icoon te verwijderen uit de werkbalk.\\n'VERPLAATS ICOON' - dit laat een lijst zien van alle toepassingen die iconen op de werkbalk hebben. Dubbel links klik op een toepassing om deze te selecteren en dan naar links of rechts te verplaatsen.\\n'GEAVANCEERD' - u kunt het tekstconfiguratiebestand met alle instellingen van de werkbalk op uw bureaublad bewerken. Door dit bestand handmatig te bewerken kan de gebruiker de volgorde van de iconen wijzigen en een willekeurig icoon verwijderen of toevoegen. Een korte uitleg over de werking van het tekstconfiguratiebestand wordt weergegeven voordat het bestand wordt geopend om te bewerken.\\nWaarschuwingen: bewerk een configuratiebestand alleen handmatig als u zeker weet wat u doet! Maak altijd een reservekopie voordat u een configuratiebestand bewerkt!"

#: icewm-toolbar-icon-manager.sh:86
msgid "Warning::TXT"
msgstr "Waarschuwing::TXT"

#: icewm-toolbar-icon-manager.sh:86
msgid ""
"If you click 'Yes', the toolbar configuration file will be opened for manual editing.\\n\n"
"How-to:\\nEach toolbar icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each toolbar icon entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nSave any changes and then restart IceWM.\\nYou can undo the last change from TIMs UNDO LAST STEP button."
msgstr ""
"Als u op 'Ja' klikt wordt de werkbalk geopend voor handmatig aanpassen.\\n\n"
"Hoe:\\nIeder werkbalkicoon wordt geïdentificeerd door een regel die begint met 'prog' gevolgd door de naam van de toepassing, het icoon, en het uitvoerbare bestand.\\n Verplaats, pas aan, of verwijder de gehele regel die met ieder werkbalkicoon te maken heeft.\\nLet op: Regels die beginnen met # zijn enkel commentaren en worden genegeerd.\\nEr kunnen leegregels bestaan.\\nSla veranderingen op en herstart IceWM.\\nU kunt de laatste verandering ongedaan maken vanuit TIMs LAATSTE STAP ONGEDAAN MAKEN knop."

#: icewm-toolbar-icon-manager.sh:87
msgid "Ok"
msgstr "Ok"

#: icewm-toolbar-icon-manager.sh:102
msgid "Double click any Application to remove its icon:"
msgstr "Dubbelklik op een Toepassing om het icoon ervan te verwijderen:"

#: icewm-toolbar-icon-manager.sh:102
msgid "Remove"
msgstr "Verwijder"

#: icewm-toolbar-icon-manager.sh:113
msgid "file has something"
msgstr "bestand heeft iets"

#: icewm-toolbar-icon-manager.sh:119
msgid "file is empty"
msgstr "bestand is leeg"

#: icewm-toolbar-icon-manager.sh:121
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Er is niets veranderd!\\nTIP: je kunt altijd de Geavanceerd knop proberen."

#: icewm-toolbar-icon-manager.sh:135
msgid "Double click any Application to move its icon:"
msgstr "Dubbelklik op een Toepassing om het icoon ervan te verplaatsen:"

#: icewm-toolbar-icon-manager.sh:135
msgid "Move"
msgstr "Verplaats"

#: icewm-toolbar-icon-manager.sh:145
msgid "nothing was selected"
msgstr "er was niets geselecteerd"

#: icewm-toolbar-icon-manager.sh:155
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Kies wat te doen met $EXEC icoon"

#: icewm-toolbar-icon-manager.sh:157
msgid "Move left"
msgstr "Beweeg naar links"

#: icewm-toolbar-icon-manager.sh:158
msgid "Move right"
msgstr "Beweeg naar rechts"

#: icewm-toolbar-icon-manager.sh:223
msgid "Add selected app's icon"
msgstr "Voeg geselecteerde app's icoon toe"

#: icewm-toolbar-icon-manager.sh:223
msgid "Choose application to add to the Toolbar"
msgstr "Kies toepassing om toe te voegen aan de Werkbalk"

#: icewm-toolbar-icon-manager.sh:339
msgid "HELP!help:FBTN"
msgstr "HELP!help:FBTN"

#: icewm-toolbar-icon-manager.sh:340
msgid "ADVANCED!help-hint:FBTN"
msgstr "GEAVANCEERD!help-hint:FBTN"

#: icewm-toolbar-icon-manager.sh:341
msgid "ADD ICON!add:FBTN"
msgstr "VOEG ICOON TOE!add:FBTN"

#: icewm-toolbar-icon-manager.sh:342
msgid "REMOVE ICON!remove:FBTN"
msgstr "VERWIJDER ICOON!remove:FBTN"

#: icewm-toolbar-icon-manager.sh:343
msgid "MOVE ICON!gtk-go-back-rtl:FBTN"
msgstr "VERPLAATS ICOON!gtk-go-back-rtl:FBTN"

#: icewm-toolbar-icon-manager.sh:344
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "LAATSTE STAP ONGEDAAN MAKEN!undo:FBTN"

#: icewm-toolbar-icon-manager.sh:345
msgid ""
"Please select any option from the buttons below to manage Toolbar icons"
msgstr ""
"Selecteer aub een optie uit de onderstaande knoppen om de Werkbalkiconen te "
"beheren"
