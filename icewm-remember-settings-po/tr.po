# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# mahmut özcan <faradundamarti@yandex.com>, 2023
# mahmut özcan <mahmutozcan@protonmail.com>, 2023
# Mehmet Akif 9oglu, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:46+0200\n"
"PO-Revision-Date: 2023-02-28 15:49+0000\n"
"Last-Translator: Mehmet Akif 9oglu, 2023\n"
"Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: icewm-remember-settings:108
msgid "HELP"
msgstr "YARDIM"

#: icewm-remember-settings:109
msgid ""
"Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a "
"window using the IceWM-remember-settings app."
msgstr ""
"IceWM-remember-settings uygulamasını kullanarak bir pencerenin <b>boyutunu "
"ve konumunu</b>, <b>çalışma alanını</b> ve <b>katmanını</b> kaydedin."

#: icewm-remember-settings:111
msgid ""
"Next time you launch the program, it will remember the window properties "
"last saved."
msgstr ""
"Programı bir sonraki başlatışınızda, en son kaydedilen pencere özelliklerini"
" hatırlayacaktır."

#: icewm-remember-settings:113
msgid "You can also delete this information unticking all options."
msgstr ""
"Ayrıca tüm seçeneklerin işaretini kaldırarak bu bilgiyi silebilirsiniz."

#: icewm-remember-settings:114
msgid ""
"Use the <b>Select other</b> option to select a different window/program to "
"configure."
msgstr ""
"Yapılandırılacak farklı bir pencere/uygulama seçmek için <b>Diğerini seç</b>"
" seçeneğini kullanın."

#: icewm-remember-settings:126 icewm-remember-settings:219
msgid "Add/Remove IceWM Window Defaults"
msgstr "IceWM Pencere Varsayılanlarını Ekle/Kaldır"

#: icewm-remember-settings:127
msgid "<b>Select a program. Store it's window properties.</b>"
msgstr "<b>Bir uygulama seçin. Pencere özelliklerini saklayın.</b>"

#: icewm-remember-settings:128
msgid "What window configuration you want antiX to remember/forget?"
msgstr ""
"AntiX’in hangi pencere yapılandırmasını hatırlamasını/unutmasını "
"istiyorsunuz?"

#: icewm-remember-settings:220
#, sh-format
msgid ""
"Entries shown below are for the <b>$appclass</b> ($appname) window.\\n\\nAll"
" options marked will be saved, all unmarked will be deleted.\\n\\n Note: "
"Workspace number shown is the window's current workspace. \\n \tDon't worry "
"that it appears too low.\\n\\n"
msgstr ""
"Aşağıda gösterilen girişler <b>$appclass</b> ($appname) penceresi "
"içindir.\\n\\nİşaretlenen tüm seçenekler kaydedilecek, işaretlenmeyenlerin "
"tümü silinecek.\\n\\n Not: Gösterilen çalışma alanı numarası, pencerenin "
"mevcut çalışma alanıdır. \\n Çok düşük göründüğü için endişelenmeyin.\\n\\n"

#: icewm-remember-settings:225
msgid "Select"
msgstr "Seç"

#: icewm-remember-settings:225
msgid "Type"
msgstr "Tür"

#: icewm-remember-settings:225
msgid "Value"
msgstr "Değer"

#: icewm-remember-settings:226
msgid "Geometry"
msgstr "Geometri"

#: icewm-remember-settings:227
msgid "Layer"
msgstr "Katman"

#: icewm-remember-settings:227
msgid "workspace"
msgstr "çalışma alanı"

#: icewm-remember-settings:229
msgid "Select other"
msgstr "Diğerini seç"
