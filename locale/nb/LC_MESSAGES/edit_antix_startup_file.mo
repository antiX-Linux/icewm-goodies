��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     8      W  4   x  �   �     5     ;  
   L  T   W  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Legg til en (manuell) kommando Legg til et program fra ei liste Skriv inn kommando som skal legges til oppstartsfila Skriv inn kommando som skal kjøres når antiX startes. Legg merke til at ampersand & vil automatisk legges til i slutten av kommandoen Fjern Fjern et program Oppstart ( Linja ble lagt til $startupfile. Den vil startes automatisk neste gang antiX startes \n $add_remove_text $startupfile): antiX Oppstartsgrensesnitt 