��          �            x  6   y      �     �     �     �  T   �  w   :     �     �     �  U   �     !  <   '  ;   d  	   �  �  �  ^   �  A        E     X     g  �   p  �     
   �     �     �  �        �  m   �  f   +	     �	                  
                                 	                            <b>Select a program. Store it's window properties.</b> Add/Remove IceWM Window Defaults Geometry HELP Layer Next time you launch the program, it will remember the window properties last saved. Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a window using the IceWM-remember-settings app. Select Select other Type Use the <b>Select other</b> option to select a different window/program to configure. Value What window configuration you want antiX to remember/forget? You can also delete this information unticking all options. workspace Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:49+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 <b>Выберите программу. Сохранить свойства ее окна.</b> Добавить/удалить стандарты окна IceWM Геометрия СПРАВКА Слой При следующем запуске программы, она вспомнит свойства окна, сохранённые в последний раз. Сохранить <b>размер и положение</b>, <b>рабочую область</b> и <b>слой</b> окна с помощью приложения IceWM-remember-settings. Выбор Выбрать другое Тип Используйте параметр <b>Выбрать другое</b>, чтобы выбрать другое окно/программу для настройки. Значение Какую конфигурацию окна вы хотите, чтобы antiX запомнил/забыл? Вы также можете удалить эту информацию, сняв все флажки. рабочее место 