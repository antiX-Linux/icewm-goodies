��          �      l      �     �     �       !   #  .   E  0   t     �  	   �     �  D   �             7   '     _     x     �      �     �     �     �  �  �  %   e  $   �  O   �  ?      �   @  �   �     ^     {     �  �   �  (   �     �  �   �  U   c	     �	     �	  Y   �	  3   D
  #   x
  &   �
     
                                                                         	                    ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Move No changes were made!\nTIP: you can always try the Advanced buttton. REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: Panwar108 <caspian7pena@gmail.com>, 2023
Language-Team: Hindi (https://app.transifex.com/anticapitalista/teams/10162/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
 आइकन जोड़ें!add:FBTN विस्तृत!help-hint:FBTN चयनित अनुप्रयोग का आइकन जोड़ें $EXEC आइकन हेतु कार्य चुनें आइकन अंतरण हेतु संबंधित अनुप्रयोग पर द्वि-क्लिक करें : आइकन हटाने हेतु संबंधित अनुप्रयोग पर द्वि-क्लिक करें : सहायता!help:FBTN सहायता::TXT अंतरित करें कोई परिवर्तन लागू नहीं किया गया!\n सहायक निर्देश : विस्तृत बटन उपयोग करके देखें। आइकन हटाएँ!remove:FBTN हटाएँ यह स्क्रिप्ट केवल आइस डब्लू-एम डेस्कटॉप में निष्पादन हेतु ही है अंतिम परिवर्तन पूर्ववत करें!undo:FBTN चेतावनी चेतावनी::TXT आइस डब्लू-एम डेस्कटॉप प्रयुक्त है फाइल में सामग्री है फाइल रिक्त है कुछ चयनित नहीं 