��          �      ,      �  6   �      �  �   �     �     �     �  T   �  w   B     �     �     �  U   �     )  <   /  ;   l  	   �  �  �  H   ~  9   �      	   �     �     �  o   �  �   	  
   �	     �	     �	  �   �	     h
  Y   n
  Z   �
     #                                  	                
                            <b>Select a program. Store it's window properties.</b> Add/Remove IceWM Window Defaults Entries shown below are for the <b>$appclass</b> ($appname) window.\n\nAll options marked will be saved, all unmarked will be deleted.\n\n Note: Workspace number shown is the window's current workspace. \n 	Don't worry that it appears too low.\n\n Geometry HELP Layer Next time you launch the program, it will remember the window properties last saved. Save the <b>size and position</b>, <b>workspace</b> and <b>layer</b> of a window using the IceWM-remember-settings app. Select Select other Type Use the <b>Select other</b> option to select a different window/program to configure. Value What window configuration you want antiX to remember/forget? You can also delete this information unticking all options. workspace Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:49+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <b>Selecione um programa para armazenar as propriedades das janelas.</b> Adicionar ou Remover os Padrões para as Janelas do IceWM As entradas exibidas abaixo são para a janela <b>$appclass</b> do aplicativo ($appname).\n\nTodas as opções marcadas ou selecionadas serão salvas/gravadas e todas as opções desmarcadas ou não selecionadas serão excluídas.\n\n Observação: O número da área de trabalho é exibido na área de trabalho atual da janela. \n 	Não se preocupe se parecer ser muito pequeno.\n\n Geometria AJUDA Camada Da próxima vez que você iniciar o programa, serão mantidas as propriedades da janela salva pela última vez. Salve o <b>tamanho</b>, a <b>posição</b>, a <b>área de trabalho</b> e a <b>camada</b> de uma janela utilizando o aplicativo de configurações ‘IceWM Remember Settings’. Selecionar Selecionar Outra Janela Tipo Utilize a opção <b>Selecionar Outra Janela</b> para selecionar uma outra janela de um outro programa diferente para ser configurado. Valor Quais configurações das janelas que você deseja que o antiX memorize ou não memorize? Você também pode excluir estas informações desmarcando todas as opções selecionadas. Área de Trabalho 