��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  ~  �     ,  #   G  7   k  �   �     0     9  	   P  n   Z  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://app.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Afegeix una ordre (manual) Afegeix una aplicació de la llista Entreu l'ordre que s'ha d'afegir al fitxer d'arrencada  Entreu l'ordre que voleu executar a l'arrencada d'antiX. Tingueu en compte que s'afegirà automàticament un ampersand/& al final de l'ordre Elimina  Elimina una aplicació Startup ( S'ha afegit aquesta línia a $startupfile. S'executarà automàticament la pròxima vegada que arrenqueu antiX \n $add_remove_text $startupfile): antiX-startup IGU 