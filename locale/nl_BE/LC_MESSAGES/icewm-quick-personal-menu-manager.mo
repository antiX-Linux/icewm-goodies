��          �      l      �     �     �       !   #  .   E  0   t     �  	   �     �  D   �             7   '     _     x     �      �     �     �     �    �     d     |  "   �      �  ?   �  ?        [  	   j  	   t  I   ~     �     �  <   �  %   -     S     `     r     �     �     �     
                                                                         	                    ADD ICON!add:FBTN ADVANCED!help-hint:FBTN Add selected app's icon Choose what do to with $EXEC icon Double click any Application to move its icon: Double click any Application to remove its icon: HELP!help:FBTN Help::TXT Move No changes were made!\nTIP: you can always try the Advanced buttton. REMOVE ICON!remove:FBTN Remove This script is meant to be run only in an IceWM desktop UNDO LAST STEP!undo:FBTN Warning Warning::TXT You are running an IceWM desktop file has something file is empty nothing was selected Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-19 14:12+0000
Last-Translator: Vanhoorne Michael, 2023
Language-Team: Dutch (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 VOEG ICOON TOE!add:FBTN GEAVANCEERD!help-hint:FBTN Voeg geselecteerde app's icoon toe Kies wat te doen met $EXEC icoon Dubbelklik op een Toepassing om het icoon ervan te verplaatsen: Dubbelklik op een Toepassing om het icoon ervan te verwijderen: HELP!help:FBTN Help::TXT Verplaats Er is niets veranderd!\nTIP: je kunt altijd de Geavanceerd knop proberen. VERWIJDER ICOON!remove:FBTN Verwijderen Dit script kan alleen worden uitgevoerd in een IceWM desktop LAATSTE STAP ONGEDAAN MAKEN!undo:FBTN Waarschuwing Waarschuwing::TXT U gebruikt een IceWM desktop bestand heeft iets bestand is leeg er was niets geselecteerd 