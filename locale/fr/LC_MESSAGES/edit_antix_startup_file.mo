��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     M  /   m  6   �  �   �  	   r     |     �  {   �  #     .   C                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (https://app.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Ajouter une commande (manuelle) Ajouter une application à partir d’une liste Saisir la commande à ajouter au fichier de démarrage Entrez la commande que vous souhaitez exécuter au démarrage d’antiX. Remarque : une esperluette /& sera automatiquement ajoutée à la fin de la commande Supprimer Supprimer une application Démarrage ( La ligne a été ajoutée au fichier $startupfile. Il démarrera automatiquement la prochaine fois que vous lancerez antiX. \n $add_remove_text $startupfile) : antiX fichier démarrage - interface graphique 