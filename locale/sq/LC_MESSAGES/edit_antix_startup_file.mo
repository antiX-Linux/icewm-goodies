��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     6  "   S  3   v  �   �     =     B     X  ^   `  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>, 2023
Language-Team: Albanian (https://app.transifex.com/anticapitalista/teams/10162/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 Shtoni një urdhër (dorazi) Shtoni aplikacione prej një liste Jepni urdhrin që duhet shtuar te kartela e nisjeve Jepni urdhrin që doni të xhirohet gjatë nisjes së antiX-it. Shënim: në fund të urdhrit do të shtohet automatikisht një simbol ampersand/& Hiqe Hiqni një aplikacion Nisje ( Rreshti u shtua te $startupfile. Do të niset automatikisht, herës tjetër që nisni antiX-in \n $add_remove_text $startupfile): GUI antiX-startup 