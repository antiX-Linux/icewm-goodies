��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  p  �          6  .   M  �   |     �            s   ,  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Mehmet Akif 9oglu, 2023
Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Bir (manuel) komut ekle Listeden uygulama ekle Başlangıç dosyasına eklenecek komutu girin antiX başlangıcında çalıştırmak istediğiniz komutu girin. Not: komutun sonuna otomatik olarak bir ve /& işareti eklenir Kaldır Bir uygulamayı kaldır Başlangıç ( Satır, $startupfile dosyasına eklendi. antiX’i bir sonraki başlatışınızda otomatik olarak başlayacaktır. \n $add_remove_text $startupfile): antiX-başlangıç Arayüzü 