��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  p  �               $  D   4  K   y     �     �     �  +   �  P     �   h     
  3   &  /   Z  .   �  *   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Mehmet Akif 9oglu, 2023
Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Uygula Geri Adı değiştir İstenen çalışma alanı adlarını girip Uygula’yı tıklayın. İstenen sayıda çalışma alanı ayarlandığında Bitti’ye tıklayın. Bitti İstenen isimleri girin: Ayrıl İstenen masaüstü sayısını ayarlayın: Çalışma Alanı adlarını değiştirmek için Adı Değiştir’e tıklayın. Fare tekerleğini kullanın veya oklara tıklayın. Klavyeyi ve ardından enter tuşunu kullanarak da bir sayı girebilirsiniz. Değişiklik hemen gerçekleşir. Çalışma Alanı Numarası aWCS antiX çalışma alanı sayısı değiştirici aWCS antiX çalışma alanı adı değiştirici antiX çalışma alanı sayısı değiştirici antiX çalışma alanı adı değiştirici 