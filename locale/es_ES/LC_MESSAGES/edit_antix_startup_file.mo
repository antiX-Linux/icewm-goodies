��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     `      |  <   �  �   �     ]     f       d   �  "   �                       
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (Spain) (https://app.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Añadir un comando (manual) Añadir aplicación de una lista Introduzca el comando que desea añadir al archivo de inicio Introduzca el comando que desea ejecutar al iniciar antiX. Nota: al final del comando se añadirá automáticamente un ampersand/& Eliminar Eliminar una aplicación Inicio ( La línea se añadió a $startupfile. Se iniciará automáticamente la próxima vez que inicie antiX \n $add_remove_text $startupfile): GUI inicio-antiX 